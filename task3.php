<?php
class MyCalculator{
  public $first_value;
    public $second_value;
    public function __construct($first_value,$second_value){
        $this->first_value=$first_value;
        $this->second_value=$second_value;
    }
    public function Add(){
       return $this->first_value + $this->second_value;
    }
    public function Subtract(){
        return $this->first_value - $this->second_value;
    }
    public function Multiply(){
        return $this->first_value * $this->second_value;
    }
    public function Division(){
        return $this->first_value / $this->second_value;
    }
}
$mycalc = new MyCalculator( 12, 6);
echo "Result of Add: ".$mycalc-> Add()."<br>";
echo "Result of Subtract: ".$mycalc-> Subtract()."<br>";
echo "Result of Multiply: ".$mycalc-> Multiply()."<br>";
echo "Result of Division: ".$mycalc-> Division()."<br>";
?>