<?php
class factorial_of_a_number
{
    protected $_number;

    public function __construct($number)
    {
        $this->_number = $number;
        if (!is_int($number))
            throw new InvalidArgumentException('Not a number or missing argument');
    }

    function result()
    {
        $factorial=1;
        for ($i = 1; $i <= $this->_number; $i++) {
            $factorial = $factorial * $i;

        }
        return $factorial;

    }
}
$newfactorial = New factorial_of_a_number(5);
echo "Factorial Number is:". $newfactorial->result();
 ?>